Name:           crip
Version:        3.9
Release:        1%{?dist}
Summary:        A terminal-based ripper/encoder/tagger tool.

License:        GPL
URL:            http://bach.dynet.com/%{name}/
Source0:        http://bach.dynet.com/%{name}/src/%{name}-%{version}.tar.gz

Requires:       cdparanoia
Requires:       flac
Requires:       perl
Requires:       sox
Requires:       vorbis-tools
Requires:       vorbisgain

%description
crip is a terminal-based ripper/encoder/tagger tool for creating Ogg
Vorbis/FLAC files under UNIX/Linux. It is well-suited for anyone (especially
the perfectionist) who seeks to make a lot of files from CDs and have them all
properly labeled and professional-quality with a minimum of hassle and yet
still have flexibility and full control. Current versions of crip support Ogg
Vorbis and FLAC.


%prep
%setup -q


%build


%install
%{__rm} -rf %{buildroot}
%{__install} -D -m0755 %{name} %{buildroot}/%{_datadir}/%{name}/%{version}/%{name}
%{__install} -D -m0755 editfilenames %{buildroot}/%{_datadir}/%{name}/%{version}/editfilenames
%{__install} -D -m0755 editcomment %{buildroot}/%{_datadir}/%{name}/%{version}/editcomment
%{__install} -D -m0644 CDDB_get.pm %{buildroot}/%{perl_vendorlib}/CDDB_get.pm
%{__install} -d -m0755 %{buildroot}/%{_bindir}
%{__ln_s} %{_datadir}/%{name}/%{version}/%{name} %{buildroot}/%{_bindir}/%{name}


%files
%doc Changelog criprc_example LICENSE README TODO
%{perl_vendorlib}/*
%{_datadir}/%{name}/%{version}/*
%{_bindir}/%{name}



%changelog
* Fri Nov 16 2012 Jon Wilson <jwlsinx@gmail.com> - 3.9-1
- Initial packaging for Fedora.
