Name:           keepassx
Version:        2.0alpha6
Release:        1%{?dist}
Summary:        Cross-platform password manager

License:        GPLv2+
URL:            https://www.keepassx.org/
Source0:        https://www.keepassx.org/dev/attachments/download/69/%{name}-2.0-alpha6.tar.gz
Source1:        keepassx2.desktop

%define __ctest /usr/bin/ctest
%define __keepassx_icons %{_datadir}/%{name}/icons
%define __desktopdir %{_datadir}/applications
%define __iconsbasedir %{_datadir}/icons/hicolor

BuildRequires:  libX11-devel
BuildRequires:  libXext-devel
BuildRequires:  libXtst-devel
BuildRequires:  libgcrypt-devel
BuildRequires:  qt-devel
Requires:       libX11
Requires:       libXext
Requires:       libXtst
Requires:       libgcrypt
Requires:       qt


%description
KeePassX is an application for people with extremly high demands on secure
personal data management.

KeePassX saves many different information e.g. user names, passwords, urls,
attachments and comments in one single database. For a better management
user-defined titles and icons can be specified for each single entry.
Furthermore the entries are sorted in groups, which are customizable as well.
The integrated search function allows to search in a single group or the
complete database.

KeePassX offers a little utility for secure password generation. The password
generator is very customizable, fast and easy to use.  Especially someone who
generates passwords frequently will appreciate this feature.

The complete database is always encrypted either with AES (alias Rijndael) or
Twofish encryption algorithm using a 256 bit key. Therefore the saved
information can be considered as quite safe. KeePassX uses a database format
that is compatible with KeePass Password Safe for MS Windows.


%prep
%setup -n keepassx-2.0-alpha6 -q


%build
%{cmake} -DFLAGS=-O2 -DCMAKE_BUILD_TYPE=Release .
%{__make} %{?_smp_mflags}


%install
rm -rf %{buildroot}
%{make_install}
%{__install} -D -m0644 %{SOURCE1} %{buildroot}/%{__desktopdir}/%{name}.desktop


%check
%{__ctest}


%post
/bin/touch --no-create %{_datadir}/mime/packages &>/dev/null || :
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
/usr/bin/update-desktop-database -q || :


%postun
if [ $1 -eq 0 ] ; then
  /usr/bin/update-mime-database %{_datadir}/mime &> /dev/null || :
  /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
  /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
  /usr/bin/update-desktop-database -q || :
fi


%files
%doc CHANGELOG COPYING LICENSE.BSD LICENSE.CC0 LICENSE.GPL-2 LICENSE.GPL-3 LICENSE.LGPL-2.1 LICENSE.LGPL-3 LICENSE.NOKIA-LGPL-EXCEPTION
%{_bindir}/%{name}
%{_libdir}/%{name}/lib%{name}-autotype-x11.so
%{_datadir}/mime/packages/%{name}.xml
%{__desktopdir}/%{name}.desktop
%{__iconsbasedir}/*/apps/%{name}.png
%{__iconsbasedir}/*/mimetypes/*.png
%{__iconsbasedir}/scalable/apps/%{name}.svgz
%{__keepassx_icons}/application/*/apps/%{name}.png
%{__keepassx_icons}/application/*/apps/%{name}.svgz
%{__keepassx_icons}/application/*/actions/*.png
%{__keepassx_icons}/application/*/status/*.png
%{__keepassx_icons}/application/*/mimetypes/*.png
%{__keepassx_icons}/database/*.png


%changelog
* Sat Dec 20 2014 Jon Wilson <jwlsinx@gmail.com> - 2.0alpha6-1
- Updated to 2.0 alpha6.

* Fri Nov 23 2012 Jon Wilson <jwlsinx@gmail.com> - 2.0alpha3-3
- Added AutoType patch support for curly braces.

* Fri Nov 23 2012 Jon Wilson <jwlsinx@gmail.com> - 2.0alpha3-2
- Added libXtst dependencies for AutoType support.
- Move AutoType shared library for x86_64 architecture.

* Thu Nov 22 2012 Jon Wilson <jwlsinx@gmail.com> - 2.0alpha3-1
- Initial packaging for Fedora.
