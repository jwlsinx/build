Name:           binreader
Version:        1.0beta3
Release:        2%{?dist}
Summary:        Binreader Usenet client.

License:        custom
URL:            http://www.binreader.com/
%ifarch x86_64
Source0:        http://www.binreader.com/%{name}_%{version}_amd64.deb
%else
Source0:        http://www.binreader.com/%{name}_%{version}_i386.deb
%endif

%define _qt4_translationdir %{_datadir}/qt4/translations
%define __versiondir %{name}-%{version}

Requires:       freetype
Requires:       glibc
Requires:       libX11
Requires:       libXdmcp
Requires:       libXrender
Requires:       libpng
Requires:       libxcb
Requires:       openssl
Requires:       par2cmdline
Requires:       qt >= 4.7
Requires:       qt-x11
Requires:       unrar


%description
Binreader Usenet client.


%prep
%setup -c -T -q
%{__ar} p %{SOURCE0} data.tar.gz | %{__tar} xz


%build
%{__sed} -i 's/Exec=binreader/Exec=binreader %%F/' .%{_datadir}/applications/binreader.desktop


%install
%{__rm} -rf %{buildroot}
%{__install} -D -m0755 .%{_bindir}/%{name} %{buildroot}/%{_bindir}/%{name}
%{__install} -D -m0644 .%{_datadir}/mime/packages/binreader.xml %{buildroot}/%{_datadir}/mime/packages/binreader.xml
%{__install} -D -m0644 .%{_datadir}/applications/binreader.desktop %{buildroot}/%{_datadir}/applications/binreader.desktop
%{__install} -D -m0644 .%{_datadir}/pixmaps/binreader.png %{buildroot}/%{_datadir}/pixmaps/binreader.png
%{__install} -D -m0644 .%{_qt4_translationdir}/binreader_nl.qm %{buildroot}/%{_qt4_translationdir}/binreader_nl.qm
%{__install} -D -m0644 .%{_qt4_translationdir}/binreader_fr.qm %{buildroot}/%{_qt4_translationdir}/binreader_fr.qm
%{__install} -D -m0644 .%{_qt4_translationdir}/binreader_de.qm %{buildroot}/%{_qt4_translationdir}/binreader_de.qm
%{__install} -D -m0644 .%{_qt4_translationdir}/qt_nl.qm %{buildroot}/%{_qt4_translationdir}/qt_nl.qm


%post
update-desktop-database -q
update-mime-database /usr/share/mime


%postun
update-desktop-database -q
update-mime-database /usr/share/mime


%files
%doc
%{_bindir}/%{name}
%{_datadir}/mime/packages/binreader.xml
%{_datadir}/applications/binreader.desktop
%{_datadir}/pixmaps/binreader.png
%{_qt4_translationdir}/*


%changelog
* Sat Nov 20 2012 Jon Wilson <jwlsinx@gmail.com> - 1.0beta3-2
- Added more dependencies.
- Added sed line to update the desktop file launcher.
- Pipe the output of ar into tar for extraction.

* Fri Nov 16 2012 Jon Wilson <jwlsinx@gmail.com> - 1.0beta3
- Initial packaging for Fedora.
