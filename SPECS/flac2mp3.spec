Name:           flac2mp3
Version:        0.3.0rc3
Release:        1%{?dist}
Summary:        a Perl script to convert FLAC files to MP3 format.

License:        GPL
URL:            http://freshmeat.net/projects/%{name}/
Source0:        http://projects.robinbowes.com/download/flac2mp3/%{name}-%{version}.tar.gz

%define __lib   lib

Requires:       perl-File-Find-Rule
Requires:       perl-File-Which
Requires:       perl-Number-Compare
Requires:       perl-Text-Glob

%description
flac2mp3 is a Perl script to convert FLAC files to MP3 format. It will process
an entire directory tree and put the MP3 files in a similar structure. Tags are
converted where possible. It will only process a file if the flac file is newer
than the MP3 file or if the tags have changed. If just the tags have changed
then just the tags are processed, i.e. the file does not have to be transcoded
again.


%prep
%setup -q


%build


%install
%{__rm} -rf %{buildroot}
%{__install} -D -m0755 %{name}.pl %{buildroot}/%{_datadir}/%{name}/%{version}/%{name}.pl
%{__install} -D -m0644 %{__lib}/Proc/ParallelLoop.pm %{buildroot}/%{perl_vendorlib}/Proc/ParallelLoop.pm
%{__install} -D -m0644 %{__lib}/MP3/Tag/ID3v1.pm %{buildroot}/%{perl_vendorlib}/MP3/Tag/ID3v1.pm
%{__install} -D -m0644 %{__lib}/MP3/Tag/ImageExifTool.pm %{buildroot}/%{perl_vendorlib}/MP3/Tag/ImageExifTool.pm
%{__install} -D -m0644 %{__lib}/MP3/Tag/Cue.pm %{buildroot}/%{perl_vendorlib}/MP3/Tag/Cue.pm
%{__install} -D -m0644 %{__lib}/MP3/Tag/ID3v2-Data.pod %{buildroot}/%{perl_vendorlib}/MP3/Tag/ID3v2-Data.pod
%{__install} -D -m0644 %{__lib}/MP3/Tag/LastResort.pm %{buildroot}/%{perl_vendorlib}/MP3/Tag/LastResort.pm
%{__install} -D -m0644 %{__lib}/MP3/Tag/File.pm %{buildroot}/%{perl_vendorlib}/MP3/Tag/File.pm
%{__install} -D -m0644 %{__lib}/MP3/Tag/CDDB_File.pm %{buildroot}/%{perl_vendorlib}/MP3/Tag/CDDB_File.pm
%{__install} -D -m0644 %{__lib}/MP3/Tag/ID3v2.pm %{buildroot}/%{perl_vendorlib}/MP3/Tag/ID3v2.pm
%{__install} -D -m0644 %{__lib}/MP3/Tag/ImageSize.pm %{buildroot}/%{perl_vendorlib}/MP3/Tag/ImageSize.pm
%{__install} -D -m0644 %{__lib}/MP3/Tag/ParseData.pm %{buildroot}/%{perl_vendorlib}/MP3/Tag/ParseData.pm
%{__install} -D -m0644 %{__lib}/MP3/Tag/Inf.pm %{buildroot}/%{perl_vendorlib}/MP3/Tag/Inf.pm
%{__install} -D -m0644 %{__lib}/MP3/Tag.pm %{buildroot}/%{perl_vendorlib}/MP3/Tag.pm
%{__install} -D -m0644 %{__lib}/Audio/FLAC/Header.pm %{buildroot}/%{perl_vendorlib}/Audio/FLAC/Header.pm
%{__install} -d -m0755 %{buildroot}/%{_bindir}
%{__ln_s} %{_datadir}/%{name}/%{version}/%{name}.pl %{buildroot}/%{_bindir}/%{name}


%files
%doc changelog.txt readme.txt todo.txt
%{_datadir}/%{name}/%{version}/%{name}.pl
%{perl_vendorlib}/Proc/ParallelLoop.pm
%{perl_vendorlib}/MP3/Tag/*
%{perl_vendorlib}/MP3/Tag.pm
%{perl_vendorlib}/Audio/FLAC/Header.pm
%{_bindir}/%{name}


%changelog
* Sat Nov 24 2012 Jon Wilson <jonwilson@outlook.com> - 0.3.0rc3
- Initial packaging for Fedora.
