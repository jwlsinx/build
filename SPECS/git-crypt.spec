Name:           git-crypt
Version:        0.4
Release:        1%{?dist}
Summary:        transparent encryption and decryption of files in a git repository

License:        GPLv3
URL:            https://www.agwa.name/projects/git-crypt/
Source0:        https://www.agwa.name/projects/git-crypt/downloads/%{name}-%{version}.tar.gz


%description
git-crypt enables transparent encryption and decryption of files in a git
repository. Files which you choose to protect are encrypted when committed, and
decrypted when checked out. git-crypt lets you freely share a repository
containing a mix of public and private content. git-crypt gracefully degrades,
so developers without the secret key can still clone and commit to a repository
with encrypted files. This lets you store your secret material (such as keys or
passwords) in the same repository as your code, without requiring you to lock
down your entire repository.

%prep
%setup -n %{name}-%{version} -q


%build
%{__make} %{?_smp_mflags}


%install
%{__install} -D -m0755 %{name} %{buildroot}%{_bindir}/%{name}
%{__rm} -rf %{buildroot}usr/lib


%files
%{_bindir}/%{name}
%doc COPYING README


%changelog
* Sun Dec 21 2014 Jon Wilson <jwlsinx@gmail.com> - 0.4-1
- Initial packaging for Fedora.
